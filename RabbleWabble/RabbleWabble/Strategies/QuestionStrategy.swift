/// Copyright (c) 2022 Razeware LLC
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
/// 
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

public protocol QuestionStrategy: class {
  // 1 title will be the title for which set of questions is selected, such as "Basic Phrases."
  var title: String { get }
  
  // 2 correctCount and incorrectCount will return the current number of correct and incorrect questions, respectively.
  var correctCount: Int { get }
  var incorrectCount: Int { get }
  
  // 3 advanceToNextQuestion() will be used to move onto the next question. If there isn’t a next question available, this method will return false. Otherwise, it will return true.
  func advanceToNextQuestion() -> Bool
  
  // 4 currentQuestion() will simply return the current question. Since advanceToNextQuestion() will prevent the user from advancing beyond the available questions, currentQuestion() will always return a Question and never be nil.
  func currentQuestion() -> Question
  
  // 5 As their method names imply, markQuestionCorrect(_:) will mark a question correct, and markQuestionIncorrect(_:) will mark a question incorrect.
  func markQuestionCorrect(_ question: Question)
  func markQuestionIncorrect(_ question: Question)
  
  // 6 questionIndexTitle() will return the “index title” for the current question to indicate progress, such as "1 / 10" for the first question out of ten total.
  func questionIndexTitle() -> String
}
